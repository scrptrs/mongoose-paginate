module.exports = (schema)->
    schema.statics.paginate = (condition = {}, select = {}, skip = 0, limit = 10, sort = false)->
        self = @
        return new Promise (resolve, reject)->
            __Schema = self.find(condition, select)
            __Schema.skip(skip)
            __Schema.limit(limit)
            
            if sort
                __Schema.sort(sort)
            
            __Schema.exec (err, documents)->
                if err
                    reject(err)
                else
                    self.find(condition).count().then((_count)->
                        resolve {
                            all: _count
                            skip: skip
                            limit: limit
                            page: Math.ceil((skip + 1) / limit) || 1
                            pages: Math.ceil(_count + 0.5 / limit) - 1 || 1
                            data: documents 
                        }
                    )